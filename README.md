# custom_component to monitor Uptime Robot monitors.
  
### This custom_component is from 0.72.0, a part of Home-Assistant.
For updated documentation see:  
https://www.home-assistant.io/components/binary_sensor.uptimerobot
  
